import java.util.Scanner;
/*
Repositorio:
https://gitlab.com/mision_tic_2022/utp/ciclo2_2021/p76/introduccion_java
*/
public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, \"World!\"");
        //Esto es un comentario de una sola linea

        /*
        Esto es un 
        comentario de 
        varias lineas
        */

        /**
         * Esto es un comentario
         * de documentación
         */

         //Declarar una variables
         int numero = 0;
         double decimal = 4.5;
         float flotante = (float)2.2;
         boolean bandera = false;
         String mensaje = "Hola mundo";
         char caracter = 'c';
         String[] arreglo;
         int[][] matriz;
         /**
          * Solicitar datos por consola
          */
          //Crear un objeto scanner          
          try(Scanner leer = new Scanner(System.in)) {
            System.out.print("Por favor ingrese un entero: ");
            //Capturar un entero
            int num = leer.nextInt();
            //Imprimir en pantalla
            System.out.println("Digitó "+num);
          } catch (Exception e) {
              //TODO: handle exception
              System.out.println("Debe de digitar un entero");
          }
          
          //Cerrar
          //leer.close();


    }
}
